<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
class Api extends REST_Controller{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
    }

    public function admin_post(){
        $this->load->model('m_admin');
        $data_admin = array(
            'nama'      => $this->post('nama'),
            'uname'     => $this->post('uname'),
            'password'  => md5($this->post('password')),
            'alamat'    => $this->post('alamat'),
            'desa'      => $this->post('desa'),
            'kecamatan' => $this->post('kecamatan'),
            'jk'        => $this->post('jk'),
            'kriteria'  => $this->post('kriteria'),
            'no_telp'   => $this->post('no_telp'),
        );

        $this->load->library('form_validation');
        $this->form_validation->set_data($data_admin);
        $this->form_validation->set_rules('uname', '\'uname\'', 'required|is_unique[admin.uname]');


        if ($this->form_validation->run()==TRUE){
            if ($this->m_admin->insert($data_admin)){
                $this->response($data_admin);
            }
            else{
                $this->response('gagal');
            }
        }
        else{
            $this->response(
                $this->_response['message'] = strip_tags(validation_errors()));
        }
    }

    public function customer_post(){
        $this->load->model('m_customer');
        $data_customer = array(
            'nama_lengkap'  => $this->post('nama_lengkap'),
            'uname'         => $this->post('uname'),
            'password'      => md5($this->post('password')),
            'alamat'        => $this->post('alamat'),
            'desa'          => $this->post('desa'),
            'kecamatan'     => $this->post('kecamatan'),
            'no_telp'       => $this->post('no_telp'),
            'email'         => $this->post('email'),
            'posisi_lat'    => $this->post('posisi_lat'),
            'posisi_lng'    => $this->post('posisi_lng')
        );

        $this->load->library('form_validation');
        $this->form_validation->set_data($data_customer);
        $this->form_validation->set_rules('uname', '\'uname\'', 'required|is_unique[customer.uname]');


        if ($this->form_validation->run()==TRUE){
            if ($this->m_customer->insert($data_customer)){
                $this->response($data_customer);
            }
            else{
                $this->response('gagal');
            }
        }
        else{
            $this->response(
                $this->_response['message'] = strip_tags(validation_errors()));
        }
    }

    public function wisata_post(){
        $this->load->model('m_wisata');
        $data_wisata = array(
            'id_admin'              => $this->post('id_admin'),
            'nama'                  => $this->post('nama'),
            'desa'                  => $this->post('desa'),
            'kecamatan'             => $this->post('kecamatan'),
            'tiket_masuk_dewasa'    => $this->post('tiket_masuk_dewasa'),
            'tiket_masuk_anak'      => $this->post('tiket_masuk_anak'),
            'biaya_parkir_motor'    => $this->post('biaya_parkir_motor'),
            'biaya_parkir_mobil'    => $this->post('biaya_parkir_mobil'),
            'biaya_parkir_bus'      => $this->post('biaya_parkir_bus'),
            'foto'                  => $this->post('foto'),
            'deskripsi'             => $this->post('deskripsi'),
            'fasilitas'             => $this->post('fasilitas'),
            'posisi_lat'            => $this->post('posisi_lat'),
            'posisi_lng'            => $this->post('posisi_lng'),
            'akses'                 => $this->post('akses'),
            'zona'                  => $this->post('zona'),
            'jam_buka'              => $this->post('jam_buka'),
            'jam_tutup'             => $this->post('jam_tutup'),
            'jenis'                 => $this->post('jenis')
        );

        $this->load->library('form_validation');
        $this->form_validation->set_data($data_wisata);
        $this->form_validation->set_rules('id_admin', '\'id_admin\'', 'required');


        if ($this->form_validation->run()==TRUE){
            if ($this->m_wisata->insert($data_wisata)){
                $this->response($data_wisata);
            }
            else{
                $this->response('gagal');
            }
        }
        else{
            $this->response(
                $this->_response['message'] = strip_tags(validation_errors()));
        }
    }

    public function detail_wisata_post(){
        $this->load->model('m_wisata');
        $id_wisata = $this->post('id_wisata');
        $data_detail_wisata = $this->m_wisata->where_id_wisata($id_wisata)->with_menu()->get();
        $this->response($data_detail_wisata);
    }

    public function all_wisata_post(){
        $this->load->model('m_wisata');

        $data_all_wisata = $this->m_wisata->get_all();

        $this->response($data_all_wisata);
    }

    public function kuliner_post(){
        $this->load->model('m_kuliner');
        $data_kuliner = array(
            'id_admin'                  => $this->post('id_admin'),
            'nama'                      => $this->post('nama'),
            'alamat'                    => $this->post('alamat'),
            'no_telp'                   => $this->post('no_telp'),
            'posisi_lat'                => $this->post('posisi_lat'),
            'posisi_lng'                => $this->post('posisi_lng'),
            'harga_tiket_parkir_motor'  => $this->post('harga_tiket_parkir_motor'),
            'harga_tiket_parkir_mobil'  => $this->post('harga_tiket_parkir_mobil'),
            'harga_tiket_parkir_bus'    => $this->post('harga_tiket_parkir_bus'),
            'foto'                      => $this->post('foto'),
            'deskripsi'                 => $this->post('deskripsi'),
            'fasilitas'              => $this->post('fasilitas'),
            'jam_buka'                  => $this->post('jam_buka'),
            'jam_tutup'                 => $this->post('jam_tutup')
        );

        $this->load->library('form_validation');
        $this->form_validation->set_data($data_kuliner);
        $this->form_validation->set_rules('id_admin', '\'id_admin\'', 'required');

        if ($this->form_validation->run()==TRUE){
            if ($this->m_kuliner->insert($data_kuliner)){
                $this->response($data_kuliner);
            }
            else{
                $this->response('gagal');
            }
        }
        else{
            $this->response(
                $this->_response['message'] = strip_tags(validation_errors()));
        }
    }

    public function all_kuliner_post(){
        $this->load->model('m_kuliner');

        $data_all_kuliner = $this->m_kuliner->get_all();

        $this->response($data_all_kuliner);
    }

    public function detail_kuliner_post(){
        $this->load->model('m_kuliner');
        $id_kuliner = $this->post('id_kuliner');
        $data_detail_kuliner = $this->m_kuliner->where_id_kuliner($id_kuliner)->with_menu()->get();
        $this->response($data_detail_kuliner);
    }

    public function menu_post(){
        $this->load->model('m_menu');
        $data_menu = array(
            'id_kuliner'    => $this->post('id_kuliner'),
            'nama'          => $this->post('nama'),
            'harga'         => $this->post('harga'),
            'foto'          => $this->post('foto'),
            'deskripsi'     => $this->post('deskripsi')
        );

        $this->load->library('form_validation');
        $this->form_validation->set_data($data_menu);
        $this->form_validation->set_rules('id_kuliner', '\'id_kuliner\'', 'required');

        if ($this->form_validation->run()==TRUE){
            if ($this->m_menu->insert($data_menu)){
                $this->response($data_menu);
            }
            else{
                $this->response('gagal');
            }
        }
        else{
            $this->response(
                $this->_response['message'] = strip_tags(validation_errors()));
        }
    }

    public function penginapan_post(){
        $this->load->model('m_penginapan');
        $data_penginapan = array(
            'id_admin'                      => $this->post('id_admin'),
            'nama'                          => $this->post('nama'),
            'alamat'                        => $this->post('alamat'),
            'no_telp'                       => $this->post('no_telp'),
            'posisi_lat'                    => $this->post('posisi_lat'),
            'posisi_lng'                    => $this->post('posisi_lng'),
            'foto'                          => $this->post('foto'),
            'deskripsi'                     => $this->post('deskripsi'),
            'fasilitas'                  => $this->post('fasilitas')
        );

        $this->load->library('form_validation');
        $this->form_validation->set_data($data_penginapan);
        $this->form_validation->set_rules('id_admin', '\'id_admin\'', 'required');

        if ($this->form_validation->run()==TRUE){
            if ($this->m_penginapan->insert($data_penginapan)){
                $this->response($data_penginapan);
            }
            else{
                $this->response('gagal');
            }
        }
        else{
            $this->response(
                $this->_response['message'] = strip_tags(validation_errors()));
        }
    }

    public function all_penginapan_post(){
        $this->load->model('m_penginapan');

        $data_all_penginapan = $this->m_penginapan->get_all();

        $this->response($data_all_penginapan);
    }

    public function detail_penginapan_post(){
        $this->load->model('m_penginapan');
        $id_penginapan = $this->post('id_penginapan');
        $data_detail_penginapan = $this->m_penginapan->where_id_penginapan($id_penginapan)->with_kamar()->get();
        $this->response($data_detail_penginapan);
    }

    public function kamar_post(){
        $this->load->model('m_kamar');
        $data_kamar = array(
            'id_penginapan' => $this->post('id_penginapan'),
            'nama'          => $this->post('nama'),
            'banyak_kamar'  => $this->post('banyak_kamar'),
            'kapasitas'     => $this->post('kapasitas'),
            'fasilitas'     => $this->post('fasilitas'),
            'harga'         => $this->post('harga')
        );

        $this->load->library('form_validation');
        $this->form_validation->set_data($data_kamar);
        $this->form_validation->set_rules('id_penginapan', '\'id_penginapan\'', 'required');

        if ($this->form_validation->run()==TRUE){
            if ($this->m_kamar->insert($data_kamar)){
                $this->response($data_kamar);
            }
            else{
                $this->response('gagal');
            }
        }
        else{
            $this->response(
                $this->_response['message'] = strip_tags(validation_errors()));
        }
    }

    public function detail_kamar_post(){
        $this->load->model('m_kamar');
        $id_kamar = $this->post('id_kamar');

        $detail_kamar = $this->m_kamar->where_id_kamar($id_kamar)->with_penginapan()->get();
        $this->response($detail_kamar);

    }

    public function recomendation_post(){

        // ex jenis_layanan = 'wisata'. hanya 1 !
        $jenis_layanan = $this->post('jenis_layanan');

        $budget_wisata = $this->post('budget_wisata');
        $budget_kamar = $this->post('budget_kamar');
        $budget_menu = $this->post('budget_menu');

        //default jumlah_xxxxx = 0
        $jumlah_motor = $this->post('jumlah_motor');
        $jumlah_mobil = $this->post('jumlah_mobil');
        $jumlah_bus = $this->post('jumlah_bus');

        // ex wisata_jenis = 'buatan,alam', default wisata_jenis = 'null'
        $wisata_jenis = $this->post('wisata_jenis');
        $wisata_jumlah_anak = $this->post('wisata_jumlah_anak');
        $wisata_jumlah_dewasa = $this->post('wisata_jumlah_dewasa');
        $wisata_waktu = $this->post('wisata_waktu');

        //default kamar_jumlah = 0
        $kamar_jumlah = $this->post('kamar_jumlah');
        $kamar_jumlah_hari = $this->post('kamar_jumlah_hari');

        //default menu_porsi = 0
        $menu_porsi = $this->post('menu_porsi');
        $menu_waktu = $this->post('menu_waktu');

        //berdasarkan budget wisata dibagi jumlah dewasa dan budget wisata dibagi anak2
        if($jenis_layanan=='wisata'){
            $this->load->model('m_wisata');

            $wisata_jenis_fix =array();
            if (strpos($wisata_jenis, 'alam')!== false) {
                array_push($wisata_jenis_fix, 'alam');
            }

            if (strpos($wisata_jenis, 'buatan')!== false) {
                array_push($wisata_jenis_fix, 'buatan');
            }
            if (strpos($wisata_jenis, 'sejaran')!== false) {
                array_push($wisata_jenis_fix, 'sejarah');
            }

            if ($wisata_jumlah_anak!=0) {
                $budget_tiket_masuk_anak = $budget_wisata / $wisata_jumlah_anak;
                //harus akses data base foreach
                // $budget_tiket_masuk_anak = ($budget_wisata - ($tiket_masuk_dewasa*$wisata_jumlah_dewasa))/$wisata_jumlah_anak;
            }
            else{
                $budget_tiket_masuk_anak = $budget_wisata;
            }
            if ($wisata_jumlah_dewasa!=0) {
                $budget_tiket_masuk_dewasa = $budget_wisata / $wisata_jumlah_dewasa;
                // $budget_tiket_masuk_dewasa = ($budget_wisata - ($tiket_masuk_anak*$wisata_jumlah_anak))/$wisata_jumlah_dewasa;
            }
            else{
                $budget_tiket_masuk_dewasa = $budget_wisata;
            }

            $data = $this->m_wisata->
            where('jenis',$wisata_jenis_fix)->
            where('tiket_masuk_dewasa <= ',$budget_tiket_masuk_dewasa)->
            where('tiket_masuk_anak <=',$budget_tiket_masuk_anak)->get_all();

            $data_recomendation = array();
            // foreach ($data as $wisata => $value) {
            //     if ($wisata_jumlah_anak!=0) {
            //         // $budget_tiket_masuk_anak = $budget_wisata / $wisata_jumlah_anak;
            //         //harus akses data base foreach
            //         $budget_tiket_masuk_anak = ($budget_wisata - ($value->tiket_masuk_dewasa*$wisata_jumlah_dewasa))/$wisata_jumlah_anak;
            //     }
            //     else{
            //         $budget_tiket_masuk_anak = $budget_wisata;
            //     }
            //     if ($wisata_jumlah_dewasa!=0) {
            //         // $budget_tiket_masuk_dewasa = $budget_wisata / $wisata_jumlah_dewasa;
            //         $budget_tiket_masuk_dewasa = ($budget_wisata - ($value->tiket_masuk_anak*$wisata_jumlah_anak))/$wisata_jumlah_dewasa;
            //     }
            //     else{
            //         $budget_tiket_masuk_dewasa = $budget_wisata;
            //     }

            //     if ($value->tiket_masuk_dewasa <= $budget_tiket_masuk_dewasa && $value->tiket_masuk_anak <= $budget_tiket_masuk_anak) {
            //         array_push($data_recomendation, $value);
            //     }
            // }

            $count = 0;
            foreach ($data as $wisata => $value) {
                if ($wisata_jumlah_anak!=0) {

                    $budget_tiket_masuk_anak = (
                        $budget_wisata - 
                        ($value->tiket_masuk_dewasa*$wisata_jumlah_dewasa) - 
                        ($value->biaya_parkir_motor*$jumlah_motor) - 
                        ($value->biaya_parkir_mobil*$jumlah_mobil) - 
                        ($value->biaya_parkir_bus*$jumlah_bus))
                    /$wisata_jumlah_anak;
                }
                else{
                    $budget_tiket_masuk_anak = $budget_wisata;
                }
                if ($wisata_jumlah_dewasa!=0) {

                    $budget_tiket_masuk_dewasa = (
                        $budget_wisata - 
                        ($value->tiket_masuk_anak*$wisata_jumlah_anak) - 
                        ($value->biaya_parkir_motor*$jumlah_motor) - 
                        ($value->biaya_parkir_mobil*$jumlah_mobil) - 
                        ($value->biaya_parkir_bus*$jumlah_bus))
                    /$wisata_jumlah_dewasa;
                }
                else{
                    $budget_tiket_masuk_dewasa = $budget_wisata;
                }
                if ($jumlah_motor!=0) {

                    $budget_tiket_parkir_motor = (
                        $budget_wisata - 
                        ($value->tiket_masuk_anak*$wisata_jumlah_anak) - 
                        ($value->tiket_masuk_dewasa*$wisata_jumlah_dewasa) - 
                        ($value->biaya_parkir_mobil*$jumlah_mobil) - 
                        ($value->biaya_parkir_bus*$jumlah_bus))
                    /$jumlah_motor;
                }
                else{
                    $budget_tiket_parkir_motor = $budget_wisata;
                }
                if ($jumlah_mobil!=0) {

                    $budget_tiket_parkir_mobil = (
                        $budget_wisata - 
                        ($value->tiket_masuk_anak*$wisata_jumlah_anak) - 
                        ($value->tiket_masuk_dewasa*$wisata_jumlah_dewasa) - 
                        ($value->biaya_parkir_motor*$jumlah_motor) - 
                        ($value->biaya_parkir_bus*$jumlah_bus))
                    /$jumlah_mobil;
                }
                else{
                    $budget_tiket_parkir_mobil = $budget_wisata;
                }
                if ($jumlah_bus!=0) {

                    $budget_tiket_parkir_bus = (
                        $budget_wisata - 
                        ($value->tiket_masuk_anak*$wisata_jumlah_anak) - 
                        ($value->tiket_masuk_dewasa*$wisata_jumlah_dewasa) - 
                        ($value->biaya_parkir_motor*$jumlah_motor) - 
                        ($value->biaya_parkir_mobil*$jumlah_mobil))
                    /$jumlah_bus;
                }
                else{
                    $budget_tiket_parkir_bus = $budget_wisata;
                }

                if ($value->tiket_masuk_dewasa <= $budget_tiket_masuk_dewasa && 
                    $value->tiket_masuk_anak <= $budget_tiket_masuk_anak && 
                    $value->biaya_parkir_motor <= $budget_tiket_parkir_motor && 
                    $value->biaya_parkir_mobil <= $budget_tiket_parkir_mobil && 
                    $value->biaya_parkir_bus <= $budget_tiket_parkir_bus) {
                    array_push($data_recomendation, $value);
                    $harga = $budget_tiket_masuk_dewasa+$budget_tiket_masuk_anak+$budget_tiket_parkir_motor+$budget_tiket_parkir_mobil+$budget_tiket_parkir_bus;
                }

                $count++;
                if ($count==15) {
                   break; 
                }
            }

            $this->response($data_recomendation);
        }

        //berdasarkan jumlah kamar dan lama menginap
        if ($jenis_layanan=='kamar') {
            $this->load->model('m_kamar');

            $budget_kamar =  $budget_kamar / ($kamar_jumlah*$kamar_jumlah_hari);

            $data_recomendation = $this->m_kamar->
            // where('harga <=',$budget_kamar)->with_penginapan()->get_all();
            where('harga <=',$budget_kamar)->with_penginapan()->paginate(15);

            $this->response($data_recomendation);
        }

        //berdasarkan porsi
        if ($jenis_layanan=='menu') {
            $this->load->model('m_menu');

            $budget_menu = $budget_menu / $menu_porsi;

            $data_recomendation = $this->m_menu->
            // where('harga <=',$budget_menu)->with_kuliner()->get_all();
            where('harga <=',$budget_menu)->with_kuliner()->paginate(15);

            $this->response($data_recomendation);
        }
    }

    public function package_recomendation_backup(){
        $list_id_wisata = $this->post('list_id_wisata');
        $list_id_kamar =  $this->post('list_id_kamar');
        $list_id_menu = $this->post('list_id_menu');

        $jenis_layanan = array('wisata','kamar','menu');

        $data_package = array();
        $data_wisata = array();
        $data_kamar = array();
        $data_menu = array();

        for ($i=0; $i < count($jenis_layanan); $i++) { 

            $data=array();
            switch ($jenis_layanan[$i]) {
                case 'wisata':
                    $m_layanan='m_wisata';
                    $list = $list_id_wisata; 
                    break;
                case 'kamar':
                    $m_layanan='m_kamar';
                    $list = $list_id_kamar;
                    break;
                case 'menu':
                    $m_layanan='m_menu';
                    $list = $list_id_menu;
                    break;
                default:
                    # code...
                    break;
            }

            $this->load->model($m_layanan);

            $list = ltrim($list,",");
            $list = explode(",", $list);
            for ($j=0; $j < count($list); $j++) { 
                array_push($data, $this->$m_layanan->where('id_'.$jenis_layanan[$i],$list[$j])->get());
            }

            switch ($jenis_layanan[$i]) {
                case 'wisata':
                    $data_wisata = $data;
                    break;
                case 'kamar':
                    $data_kamar = $data;
                    break;
                case 'menu':
                    $data_menu = $data;
                    break;
                default:
                    # code...
                    break;
            }
        }

        $this->response(array($data_wisata,$data_kamar,$data_menu));
    }


    public function package_recomendation_post(){
        $jenis_layanan = $this->post('jenis_layanan');
        $list_id_wisata = $this->post('list_id_wisata');
        $list_id_kamar =  $this->post('list_id_kamar');
        $list_id_menu = $this->post('list_id_menu');

        switch ($jenis_layanan) {
            case 'wisata':
                $m_layanan='m_wisata';
                $list = $list_id_wisata; 
                break;
            case 'kamar':
                $m_layanan='m_kamar';
                $list = $list_id_kamar;
                break;
            case 'menu':
                $m_layanan='m_menu';
                $list = $list_id_menu;
                break;
            default:
                # code...
                break;
        }

        $this->load->model($m_layanan);

        $data=array();
        $list = ltrim($list,",");
        $list = explode(",", $list);
        for ($j=0; $j < count($list); $j++) { 

            if ($jenis_layanan=='wisata'){
                array_push($data,$this->$m_layanan->where('id_'.$jenis_layanan,$list[$j])->get());
            }

            if ($jenis_layanan=='kamar') {
                array_push($data,$this->m_kamar->where_id_kamar($list[$j])->with_penginapan()->get());
            }

            if ($jenis_layanan=='menu') {
                array_push($data,$this->$m_layanan->where('id_'.$jenis_layanan,$list[$j])->with_kuliner()->get());
            }
        }

        $this->response($data);
    }

}

    